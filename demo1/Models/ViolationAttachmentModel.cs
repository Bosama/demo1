﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace demo1.Models
{
    [Table("ViolationAttachment")]
    public class ViolationAttachmentModel
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ViolationAttachmentID { get; set; } // int, not null

        public int ViolationDetailID { get; set; } // int, not null

        [MaxLength(255)]
        [StringLength(255)]
        public string Filename { get; set; } // nvarchar(255), null

        [MaxLength(255)]
        [StringLength(255)]
        public string FilenameOrig { get; set; } // nvarchar(255), null

        [MaxLength(500)]
        [StringLength(500)]
        public string Note { get; set; } // nvarchar(500), null

        public DateTime? Created { get; set; } // datetime, null

        [MaxLength(50)]
        [StringLength(50)]
        public string CreatedBy { get; set; } // nvarchar(50), null

        public DateTime? Modified { get; set; } // datetime, null

        [MaxLength(50)]
        [StringLength(50)]
        public string ModifiedBy { get; set; } // nvarchar(50), null

        public bool Deleted { get; set; } // bit, not null

    }
}