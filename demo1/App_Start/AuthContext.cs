﻿using demo1.Controllers;
using demo1.Models;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace demo1.App_Start
{
    public class AuthContext: IdentityDbContext<IdentityUser>
    {
        public AuthContext()
            : base("AuthContext")
        {

        }
        public virtual DbSet<ViolationAttachmentModel> ViolationAttachmentModels { get; set; }


    }
}