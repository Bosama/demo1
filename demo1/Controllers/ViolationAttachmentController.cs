﻿using demo1.App_Start;
using demo1.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace demo1.Controllers
{
    [Authorize]
    [RoutePrefix("api/ViolationAttachment")]
    public class ViolationAttachmentController : ApiController
    {
        AuthContext ctx = new AuthContext();
        [Route("item")]
        [HttpGet]
        public IHttpActionResult get()
        {
            var data = new List<ViolationAttachmentModel>();
            data = ctx.ViolationAttachmentModels.Select(x => x).ToList();
            return Ok(data);
        }
        [Route("insert"), HttpPost]
        public IHttpActionResult insert(ViolationAttachmentModel violationAttachment)
        {
            try
            {
                ctx.ViolationAttachmentModels.Add(violationAttachment);
                ctx.SaveChanges();
                return Ok();
            }
            catch
            {
                return BadRequest();
            }
        }
        [Route("update"), HttpPost]
        public IHttpActionResult update(ViolationAttachmentModel violationAttachment)
        {
            try
            {
                var data = ctx.ViolationAttachmentModels.Where(x => x.ViolationAttachmentID == violationAttachment.ViolationAttachmentID).FirstOrDefault();
                data.ViolationDetailID = violationAttachment.ViolationDetailID;
                data.Filename = violationAttachment.Filename;
                data.FilenameOrig = violationAttachment.FilenameOrig;
                data.Note = violationAttachment.Note;
                data.Created = DateTime.Now;
                data.CreatedBy = violationAttachment.CreatedBy;
                data.Modified = DateTime.Now;
                data.ModifiedBy = violationAttachment.ModifiedBy;
                data.Deleted = violationAttachment.Deleted;
                ctx.SaveChanges();
                return Ok();
            }
            catch
            {
                return BadRequest();
            };
        }
        [Route("delete"), HttpDelete]
        public IHttpActionResult delete(int id)
        {
            try
            {
                var data = ctx.ViolationAttachmentModels.Where(x => x.ViolationAttachmentID == id).FirstOrDefault();
                data.Deleted = true;
                ctx.SaveChanges();
                return Ok();
            }
            catch
            {
                return BadRequest();
            }
        }
    }
}
